// STARTING COIN OBJECT
const coin = {
    state: 0, 
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        this.state = Math.floor(Math.random()*2)
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
        if(this.state == 0) {
            return "Heads" 
        } else {
            return "Tails"
        }
    },
    toHTML: function() {
        const image = document.createElement('img');
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        if(this.state == 0) {
            image.src = "heads.png";
        } else {
            image.src = "tails.png";
        }
        return image;
    }
};


// FLIP COIN 20XS
function flippingCoinTwentyTimes() {
    for(let numberOfFlips = 1; numberOfFlips <= 20; numberOfFlips++) {
        // get the flip function from coin object
        coin.flip();
        let flipResult = document.createElement("span");
        flipResult.textContent = coin.toString();
        document.body.appendChild(flipResult);
        document.body.appendChild(coin.toHTML());
    }
}
flippingCoinTwentyTimes();


// NEW COINS SHOW UP
function refreshCoins() {
    location.reload();
}